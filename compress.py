#! /usr/bin/env python3

import subprocess
import shlex
import json
import glob


# function to find the resolution of the input video file
def find_video_metadata(video_file):
    cmd = "ffprobe -v quiet -print_format json -show_streams -show_chapters"
    args = shlex.split(cmd)
    args.append(video_file)
    # run the ffprobe process, decode stdout into utf-8 & convert to JSON
    ffprobe_output = subprocess.check_output(args).decode('utf-8')
    return json.loads(ffprobe_output)


def ffmpeg_extract_audio(input_file, workdir, stream_id, language, codec_name):
    output_file = "{0}.{1}-{2}.{3}".format(os.path.basename(input_file), stream_id, language, codec_name)
    logfile = os.path.join(workdir, "{0}.dump.log".format(output_file))
    cmd = "ffmpeg -i \"{0}\" -map 0:{1} -acodec copy \"{2}\"".format(input_file, stream_id, output_file)
    #print(cmd)
    shellcmd = shlex.split(cmd)
    output = subprocess.check_output(shellcmd, cwd=workdir, stderr=subprocess.STDOUT).decode('utf-8')
    with open(logfile, 'w') as out:
        out.write(output)
    return output_file


def ffmpeg_encode_audio(input_file, workdir):
    output_file = "{0}.{1}".format(os.path.basename(input_file), 'aac')
    logfile = os.path.join(workdir, "{0}.encode.log".format(output_file))
    cmd = "ffmpeg -i \"{0}\" -vn -acodec aac -strict -2  -q:a 1 \"{1}\"".format(input_file, output_file)
    # print(cmd)
    shellcmd = shlex.split(cmd)
    output = subprocess.check_output(shellcmd, cwd=workdir, stderr=subprocess.STDOUT).decode('utf-8')
    with open(logfile, 'w') as out:
        out.write(output)


def encode_x264(input_file, workdir, bitrate, downscale):
    output_file = "{0}.{1}".format(os.path.basename(input_file), '264')
    pass1_logfile = os.path.join(workdir, "{0}.encode.pass1.log".format(output_file))
    pass2_logfile = os.path.join(workdir, "{0}.encode.pass2.log".format(output_file))
    print(" Cleaning ffmpeg temp files...")
    ffmpeg_files = glob.glob(os.path.join(workdir, 'ffmpeg2pass*'))
    for file in ffmpeg_files:
        os.remove(file)
    print(" Done")
    scale_option = ""
    if downscale:
        scale_option = "-vf scale=1080:-2"
    print(" Launching first pass...")
    cmd = "ffmpeg -y -i \"{0}\" -c:v libx264 -preset slow -b:v {1}k -an {2} " \
          "-f rawvideo -pass 1 /dev/null".format(input_file, bitrate, scale_option)
    print(cmd)
    shellcmd = shlex.split(cmd)
    output = ''
    try:
        output = subprocess.check_output(shellcmd, cwd=workdir, stderr=subprocess.STDOUT).decode('utf-8')
    except subprocess.CalledProcessError as e:
        print(" Command terminated with errors. Check log file {0}".format(os.path.basename(pass1_logfile)))
        output = e.output.decode('utf-8')
    finally:
        with open(pass1_logfile, 'w') as out:
            out.write(output)
    print(" Done")
    print(" Launching second pass...")
    cmd = "ffmpeg -y -i \"{0}\" -c:v libx264 -preset slow -b:v {1}k -an {2} " \
          "-f rawvideo -pass 2 \"{3}\"".format(input_file, bitrate, scale_option, output_file)
    print(cmd)
    shellcmd = shlex.split(cmd)
    output = ''
    try:
        output = subprocess.check_output(shellcmd, cwd=workdir, stderr=subprocess.STDOUT).decode('utf-8')
    except subprocess.CalledProcessError as e:
        print(" Command terminated with errors. Check log file {0}".format(os.path.basename(pass2_logfile)))
        output = e.output.decode('utf-8')
    finally:
        with open(pass2_logfile, 'w') as out:
            out.write(output)
    print(" Done")


def sanitize(input_string):
    allowed_cars = list(string.ascii_letters) + list(string.digits)
    result = ''
    for car in input_string:
        if car in allowed_cars:
            result += car
    return result


def mkv_extract_sub(input_file, workdir, stream_id, codec, language, forced, title):
    output_file = "{0}.{1}.{2}.{3}.{4}.{5}".format(sanitize(os.path.basename(input_file)),
                                               language if language is not None else 'nolang',
                                               stream_id,
                                               'forced' if forced else 'notforced',
                                               sanitize(title) if title is not None else 'notitle',
                                               codec,)
    logfile = os.path.join(workdir, "{0}.dump.log".format(output_file))
    cmd = "mkvextract tracks \"{0}\" {1}:{2}".format(input_file, stream_id, output_file)
    # print(cmd)
    shellcmd = shlex.split(cmd)
    output = subprocess.check_output(shellcmd, cwd=workdir, stderr=subprocess.STDOUT).decode('utf-8')
    with open(logfile, 'w') as out:
        out.write(output)
    return output_file

def mkv_extract_chapters(input_file, workdir):
    output_file = "chapters.xml"
    cmd = "mkvextract chapters \"{0}\"".format(input_file)
    shellcmd = shlex.split(cmd)
    output = subprocess.check_output(shellcmd, cwd=workdir, stderr=subprocess.STDOUT).decode('utf-8')
    with open(os.path.join(workdir, "chapters.xml"), 'w') as out:
        out.write(output)
    return output_file

def mp4_extract_chapters(input_file, workdir):
    # output_file = "chapters.xml"
    cmd = "mp4chaps --export -C \"{0}\"".format(input_file)
    shellcmd = shlex.split(cmd)
    output = subprocess.check_output(shellcmd, cwd=workdir, stderr=subprocess.STDOUT).decode('utf-8')
    # return output_file

if __name__ == '__main__':
    import argparse
    import os
    import string

    parser = argparse.ArgumentParser(description='Compress file using ffmpeg')
    parser.add_argument('file', help='file to compress')
    parser.add_argument('--workdir',
                        help='folder used to store temp file',
                        default='/media/marc/Données/Videos/000_A_compresser/')
    parser.add_argument('--audio-only',
                        help='Don\'t extract and compress audio streams',
                        action='store_true',
                        default=False,
                        dest='audio_only')
    parser.add_argument('--video-only',
                        help='Don\'t extract and compress video stream',
                        action='store_true',
                        default=False,
                        dest='video_only')
    parser.add_argument('--sub-only',
                        help='Extract only subtitles',
                        action='store_true',
                        default=False,
                        dest='sub_only')
    parser.add_argument('--chapters-only',
                        help='Extract only chapters',
                        action='store_true',
                        default=False,
                        dest='chapters_only')
    args = parser.parse_args()

    do_video = not (args.audio_only or args.sub_only or args.chapters_only)
    do_audio = not (args.video_only or args.sub_only or args.chapters_only)
    do_sub = not (args.audio_only or args.video_only or args.chapters_only)
    do_chapters = not (args.audio_only or args.video_only or args.sub_only)

    sanitized_filename = ''.join([c for c in os.path.basename(args.file)
                                 if c in string.ascii_letters + string.digits + '.'])
    file_absolute = os.path.abspath(args.file)

    metadata = find_video_metadata(args.file)
    audio_streams = [t for t in metadata['streams']
                     if t['codec_type'] == 'audio']
    video_streams = [t for t in metadata['streams']
                     if t['codec_type'] == 'video']
    sub_streams = [t for t in metadata['streams']
                     if t['codec_type'] == 'subtitle']
    has_chapters = len(metadata['chapters']) > 0

    print("################## Processing file {0}".format(os.path.basename(args.file)))
    print("file is containing {0} audio stream(s) and {1} video stream(s)"
          .format(len(audio_streams), len(video_streams)))

    print("\n### Audio streams")
    for stream in audio_streams:
        stream['language'] = stream.get('tags', {'language': 'nolang'}).get('language', 'nolang')
        print("  - ID: {0} - codec:{1} - lang:{2}"
              .format(stream['index'],
                      stream['codec_name'],
                      stream['language']))

    print("\n### Video streams")
    for stream in video_streams:
        print("  - ID: {0} - codec:{1} - resol:{2}*{3}"
              .format(stream['index'],
                      stream['codec_name'],
                      stream['width'],
                      stream['height'],))
    print("\n### Subtitle streams")
    for stream in sub_streams:
        print("  - ID: {0} - language:{1} - codec : {2} - title : {3} - forced:{4}"
              .format(stream['index'],
                      stream['tags']['language'],
                      stream['codec_name'],
                      stream['tags']['title'] if 'title' in stream['tags'] else '',
                      stream['disposition']['forced'],))

    print("\n### Chapters")
    print("  Found {0} chapters".format(len(metadata['chapters'])))

    workdir = os.path.join(args.workdir, os.path.splitext(sanitized_filename)[0])
    if not os.path.isdir(workdir):
        print("\n### Creating work-dir {0}".format(workdir))
        os.makedirs(workdir)

    if do_audio:
        print("\n### Extracting audio streams")
        for stream in audio_streams:
            print(" Extracting stream {0}...".format(stream['index']))
            stream['extracted_file'] = ffmpeg_extract_audio(file_absolute,
                                                            workdir,
                                                            stream['index'],
                                                            stream['language'],
                                                            stream['codec_name'])
            print(" Done\n")

        print("\n### Encoding audio streams")
        for stream in audio_streams:
            if stream['codec_name'] == 'aac':
                print(" Origin audio stream already aac so we don't try to compress it")
            else:
                print(" Encoding file {0}...".format(stream['extracted_file']))
                ffmpeg_encode_audio(stream['extracted_file'],
                                    workdir,)
            print(" Done\n")
        print("Done\n")

    if do_video:
        print("\n### Encoding video stream")
        encode_x264(file_absolute,
                    workdir,
                    768,
                    int(video_streams[0]['width']) > 1080,)
        print("Done\n")

    if do_sub:
        print("\n### Extracting subtitles")
        for stream in sub_streams:
            print(" Extracting stream {0}...".format(stream['index']))
            mkv_extract_sub(file_absolute,
                            workdir,
                            stream['index'],
                            stream['codec_name'],
                            stream['tags']['language'],
                            stream['disposition']['forced'],
                            stream['tags']['title'] if 'title' in stream['tags'] else '',)
        print("Done\n")

    if do_chapters:
        print("\n### Extracting chapters")
        if has_chapters and os.path.splitext(file_absolute)[1] == '.mkv':
            mkv_extract_chapters(file_absolute, workdir)
        elif has_chapters and os.path.splitext(file_absolute)[1] == '.mp4':
            mp4_extract_chapters(file_absolute, workdir)
        else:
            open(os.path.join(workdir, 'no_chapters'), 'a').close()
        print("Done\n")
